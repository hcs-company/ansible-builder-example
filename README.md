# ansible-builder / ansible-runner demo

## build
```bash
ansible-builder build -v3 --tag ansible-builder-example:demo
```

## run
```bash
ansible-runner --debug run --container-image ansible-builder-example:demo -p example-playbook.yaml --process-isolation --container-volume-mount .:/runner/project .
```

## run in navigator
```bash
ansible-navigator run --eei ansible-builder-example:demo example-playbook.yaml
```
